//
//  recordCell.swift
//  MembacaBersamaEja
//
//  Created by Candra Sabdana Nugroho on 03/06/20.
//  Copyright © 2020 William Sebastian Thedja. All rights reserved.
//

import Foundation
import UIKit

protocol RecordCellDelegate: class {
    func delete(cell: RecordCell)
}

class RecordCell: UICollectionViewCell {
    
    @IBOutlet weak var recordImage: UIImageView!
    @IBOutlet weak var nameRecordLabel: UILabel!
    @IBOutlet weak var recordTimeLabel: UILabel!
    
    weak var delegate: RecordCellDelegate?
   
    @IBAction func deleteButtonDidTap(_ sender: Any) {
       delegate?.delete(cell: self)
    }
    
}
