//
//  PopUpModalStoryViewController.swift
//  tes
//
//  Created by Candra Sabdana Nugroho on 25/06/20.
//  Copyright © 2020 afitra mamor. All rights reserved.
//

import UIKit

class PopUpModalStoryViewController: UIViewController {

    @IBOutlet weak var titleLabelObject: UILabel!
    @IBOutlet weak var imageObjectGIF: UIImageView!
    
    @IBOutlet weak var caraMengejaButton: UIButton!
    @IBOutlet weak var sudahBisaButton: UIButton!
    
    @IBOutlet weak var viewPopUp: UIView!
    
    var wordObject = ""
    var imageWordObjectName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewPopUp.layer.cornerRadius = 30
        
        imgChange()
        
        caraMengejaButton.backgroundColor = getUIColor(hex: "9261EE")
        caraMengejaButton.layer.cornerRadius = 20
        caraMengejaButton.setTitleColor(UIColor.white, for: .normal)
        
        sudahBisaButton.backgroundColor = getUIColor(hex: "999999")
        sudahBisaButton.layer.cornerRadius = 20
        sudahBisaButton.setTitleColor(UIColor.white, for: .normal)
        
        titleLabelObject.text = wordObject
        
//        print(wordObject, "######")
//        print(imageWordObjectName, ">>>>>>")
        // Do any additional setup after loading the view.
    }
    
    func imgChange() {
        let tempPurple = "-ungu"
        let tempYellow = "-kuning"
        let tempRed = "-merah"
        
        if wordObject.lowercased() == "asyik" {
            let img = wordObject.lowercased()+tempPurple
            imageObjectGIF.image = UIImage(named: img)
        }
        else if wordObject.lowercased() == "baju" {
            let img = wordObject.lowercased()+tempYellow
            imageObjectGIF.image = UIImage(named: img)
        }
        else if wordObject.lowercased() == "ransel" {
            let img = wordObject.lowercased()+tempYellow
            imageObjectGIF.image = UIImage(named: img)
        }
        else if wordObject.lowercased() == "tenda" {
            let img = wordObject.lowercased()+tempRed
            imageObjectGIF.image = UIImage(named: img)
        }
    }
    
    func getUIColor(hex: String, alpha: Double = 1.0) -> UIColor? {
        var cleanString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cleanString.hasPrefix("#")) {
            cleanString.remove(at: cleanString.startIndex)
        }
        
        if ((cleanString.count) != 6) {
            return nil
        }
        
        var rgbValue: UInt32 = 0
        Scanner(string: cleanString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    @IBAction func caraMengejaDidTap(_ sender: Any) {
        let playGifEja = "eja-"
        let playGifReal = playGifEja+wordObject.lowercased()
        
        let playGif = UIImage.gif(name: playGifReal)
        let imageView = imageObjectGIF

        imageView!.animationImages = playGif?.images
        imageView?.animationDuration = playGif!.duration
        imageView?.animationRepeatCount = 1
        imageView!.startAnimating()
    }
    
//    @IBAction func sudahBisaDidTap(_ sender: Any) {
////        if let presenter = presentingViewController as? NarasiVIewController {
////            presenter.transitionScene = "true"
////           }
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let showDismiss = storyBoard.instantiateViewController(withIdentifier: "NarasiViewController") as! NarasiVIewController
//        showDismiss.transitionScene = true
//        dismiss(animated: true, completion: nil)
//    }
}
