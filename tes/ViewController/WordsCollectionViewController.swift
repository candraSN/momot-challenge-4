//
//  WordsCollectionViewController.swift
//  MembacaBersamaEja
//
//  Created by Candra Sabdana Nugroho on 28/05/20.
//  Copyright © 2020 William Sebastian Thedja. All rights reserved.
//

import UIKit

class WordsCollectionViewController: UIViewController, UICollectionViewDelegateFlowLayout
{
    
    @IBOutlet weak var wordsCollectionViewCategories: UICollectionView!
    @IBOutlet weak var totalWordLabel: UILabel!
    
    var imageWordsCollectionCategories : [WordsCollectionCategory] = ImageLibrary.catchData()
    
    var wordImageName = ""
    var wordHeaderDetail = ""
    var wordSpellDetail = ""
    
    //private let spacing:CGFloat = 16.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        wordsCollectionViewCategories.delegate = self
        wordsCollectionViewCategories.dataSource = self
        
        let layout = wordsCollectionViewCategories.collectionViewLayout as! UICollectionViewFlowLayout
        
        layout.sectionInset = UIEdgeInsets(top: 5, left: 0, bottom: 20, right: 250)
        
        let totalWords = Helpers().retrieve().count
        let totalString = "Total: \(totalWords) kata"
        //let newTotal = totalString.replacingOccurrences(of: " 0", with: "4", options: .literal, range: nil)
        totalWordLabel.text = totalString
        
        //imageWordsCollectionCategories[0].spellingWord.append("test")
        
        //print(imageWordsCollectionCategories)
        //        for family: String in UIFont.familyNames
        //        {
        //            print(family)
        //            for names: String in UIFont.fontNames(forFamilyName: family)
        //            {
        //                print("== \(names)")
        //            }
        //        }
        
    }
    
}

extension WordsCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return imageWordsCollectionCategories.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let indexItemWord = imageWordsCollectionCategories[section].wordsImageNameCollection.count
        
        return indexItemWord
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
//        let totalWord = imageWordsCollectionCategories[indexPath.section].wordsImageNameCollection[indexPath.item].count
//
//        totalWordLabel.text = "Total: \(totalWord) Kata"
        
        
        var con = 0
        
        var huruf = imageWordsCollectionCategories[indexPath.section].alphabetCategoryTitle.lowercased()
        var kata = imageWordsCollectionCategories[indexPath.section].objectWord[indexPath.item].lowercased()
        var allCore = Helpers().retrieve()
        //        var temp = allCore
        //        var awal = allCore[indexPath.section].objekName[ allCore[indexPath.section].objekName.startIndex].lowercased()
        let cell = wordsCollectionViewCategories.dequeueReusableCell(withReuseIdentifier: "ImageWordsCell", for: indexPath) as! WordsCollectionViewCell
        var flag = false
        for (i,item ) in allCore.enumerated(){
            
            if(kata == item.objekName){
                print(kata, "====", item.objekName)
                con = i
                
                flag = true
            }
        }
        
        if(flag){
            let imageCategory = imageWordsCollectionCategories[indexPath.section]
            let imageNames = imageCategory.wordsImageNameCollection
            
            let imageName = imageNames[indexPath.item]
            print(imageName,"<<<<")
            cell.imageName = imageName
        }else{
            cell.imageName = "Rectangle"
            //            cell.isHidden = true
        }

        //cell.isHidden = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let sectionHeaderView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeaderView", for: indexPath) as! SectionHeaderView
        
        let category = imageWordsCollectionCategories[indexPath.section]
        
        sectionHeaderView.categoryTitle = category.alphabetCategoryTitle
        
        
        
        //        if category.alphabetCategoryTitle == "B" {
        //            sectionHeaderView.isHidden = true
        //        }
        
        
        return sectionHeaderView
        
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//           let numberOfItemsPerRow:CGFloat = 3
//           let spacingBetweenCells:CGFloat = 16
//
//           let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
//
//           if let collection = self.wordsCollectionViewCategories {
//               let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
//               return CGSize(width: width, height: width)
//           }else{
//               return CGSize(width: 0, height: 0)
//           }
//       }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let showDetail = storyboard.instantiateViewController(withIdentifier: "detailWordsCollection") as! DetailWordsCollectionController
        
        let cellIndex = indexPath.section
        let colloumIndex = indexPath.item
 
        
         
        
        if imageWordsCollectionCategories[cellIndex].wordsImageNameCollection[colloumIndex] == "asyik-default" {
//            showDetail.wordImage = "asyik"
            wordImageName = "asyik"
        }
        else if imageWordsCollectionCategories[cellIndex].wordsImageNameCollection[colloumIndex] == "baju-default" {
//            showDetail.wordImage = "baju"
            wordImageName = "baju"
        }
        else if imageWordsCollectionCategories[cellIndex].wordsImageNameCollection[colloumIndex] == "ransel-default" {
//            showDetail.wordImage = "ransel"
            wordImageName = "ransel"
        }
        else if imageWordsCollectionCategories[cellIndex].wordsImageNameCollection[colloumIndex] == "tenda-default" {
//            showDetail.wordImage = "tenda"
            wordImageName = "tenda"
        }
        
        wordHeaderDetail = imageWordsCollectionCategories[cellIndex].objectWord[colloumIndex]
        wordSpellDetail = imageWordsCollectionCategories[cellIndex].spellingWord[colloumIndex]
        //showDetail.wordImage = imageWordsCollectionCategories[cellIndex].wordsImageNameCollection[colloumIndex]
        
        
        var flag = false
        let allCore = Helpers().retrieve()
        for (i,item) in allCore.enumerated(){
            
            
            let asli = imageWordsCollectionCategories[cellIndex].objectWord[colloumIndex].lowercased()
            let banding = item.objekName.lowercased()
            
            if(asli == banding){
                flag = true
            }
            
        }
        
        
        if(flag){
//            showDetail.shouldPerformSegue(withIdentifier: "toDetailObject", sender: self)
//            self.present(showDetail, animated: true, completion: nil)
            performSegue(withIdentifier: "toDetailObject", sender: self)
        }
        
//        print(imageWordsCollectionCategories[cellIndex].objectWord[colloumIndex],">>>")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
               
        if segue.identifier == "toDetailObject" {
            let showDetail = segue.destination as! DetailWordsCollectionController
            showDetail.wordImage = wordImageName
             
            showDetail.wordHeader = wordHeaderDetail
            showDetail.spellWordHeader = wordSpellDetail
        }
    }
    
}
    
    

