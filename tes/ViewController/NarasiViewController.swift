//
//  NarasiViewController.swift
//  tes
//
//  Created by afitra mamor on 01/06/20.
//  Copyright © 2020 afitra mamor. All rights reserved.
//

import Foundation
import CoreData

import UIKit


class NarasiVIewController: UIViewController {
    
    var storyData:(
        isRead:Int,
        storyTitle:String,
        storyImage:String,
        wordColletion:[
        (
        photo:String,
        wordTitle: String,
        iconImage: String,
        isGif: String)],
        scene:[(
        isRead:Bool,
        finish:Bool,
        navigationNext:Bool,
        navigationBack:Bool,
        narasi:Bool,
        dialogAyah:Bool,
        objekModal:Bool,
        choise:Bool,
        isBackground:String,
        isNarasi:String,
        isDialogAyah:String,
        isChoise:(
        setIndex:[Int],
        changeBackground:[String],
        toChange:Bool,
        pattern:[
        (
        type:String,
        iconImage:String,
        setBackground:String,
        wordTitle:String
        )
        ]
        ),
        isModal:
        (
        wordTitle:String,
        photo:String,
        isGif:String,
        iconImage:String,
        containerImage:[String]))]) = (isRead:0, storyTitle:"Kamping",storyImage:"kamping",wordColletion: [],scene:[])
    
    
    @IBOutlet weak var objekModal: UIButton!
    @IBOutlet weak var navigationBack: UIButton!
    
    @IBOutlet weak var navigationNext: UIButton!
    
    @IBOutlet weak var isNarasi: UILabel!
    
    
    
    @IBOutlet weak var isBackground: UIImageView!
    
    @IBOutlet weak var choiseB: UIButton!
    @IBOutlet weak var choiseA: UIButton!
    
    @IBOutlet weak var boxDialogAyah: UIImageView!
    @IBOutlet weak var dialogAyah: UILabel!
    
    
    var indexScene:Int = 0
    var getChoise:String = ""
    var transitionScene: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        let imageNext = UIImage(named: "button-next")
        let imageBack = UIImage(named: "button-back")
        self.navigationNext.setImage(imageNext, for: .normal)
        self.navigationNext.setTitle("", for: .normal)
        self.navigationBack.setImage(imageBack, for: .normal)
        self.navigationBack.setTitle("", for: .normal)
        
        //self.choiseA.setImage(imageNext, for: .normal)
        //transitionModalPopUp()
        _ = getData()
        _ = playStory()
        
        
        print(transitionScene, "$$$$$")
        // let imageA  = UIImage(named: "baju_merah")
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goFinish" {
            let data = segue.destination as! FinishStoryViewController
            
            
            for (i,item) in storyData.wordColletion.enumerated(){
                data.listObjek.append((photo: item.photo, wordTitle: item.wordTitle, iconImage: item.iconImage, isGif: item.isGif))
            }
            
            data.myPhoto = storyData.storyImage
            
        }
        
        else if segue.identifier == "popUpModal" {
            let changeScene  = segue.destination as! PopUpModalStoryViewController
                       changeScene.wordObject = storyData.scene[indexScene].isModal.wordTitle
                       changeScene.imageWordObjectName = storyData.scene[indexScene].isModal.isGif
            //transitionScene = false
        }
    }
    
    @IBAction func unwindToScreenBefore(_ unwindSegue: UIStoryboardSegue) {
           transitionModalPopUp()
       }
    
    func transitionModalPopUp() {
        
            storyData.wordColletion.append((photo:storyData.scene[indexScene].isModal.photo,wordTitle: storyData.scene[indexScene].isModal.wordTitle, iconImage: storyData.scene[indexScene].isModal.photo, isGif: storyData.scene[indexScene].isModal.isGif))
               
            let name = "record-" + storyData.scene[indexScene].isModal.wordTitle.lowercased()
               
               Helpers().create(storyData.scene[indexScene].isModal.wordTitle.lowercased(), true)
               
               indexScene += 1
               playStory()
            transitionScene = false
        
    }
    
    @IBAction func selectB(_ sender: Any) {
        getChoise = "B"
        changBackground()
        indexScene += 1
        
        playStory()
    }
    @IBAction func selectA(_ sender: Any) {
        getChoise = "A"
        changBackground()
        
        indexScene += 1
       
        
        playStory()
    }
    @IBAction func backScene(_ sender: Any) {
        indexScene -= 1
     
        playStory()
    }
    
    @IBAction func selectObject(_ sender: Any) {
            
           
        
        
    }
    
    @IBAction func nextScene(_ sender: Any) {
        var last = storyData.scene.endIndex - 1
        
        
        if(indexScene == last){
            
            performSegue(withIdentifier: "goFinish", sender: sender)
        }else{
            indexScene += 1
            playStory()
        }
        
    }

    
    func saveIndex() ->Bool{
        let defaults = UserDefaults.standard
                 var cek = defaults.object(forKey: "indexScene") as? Int ?? 0
        
        print("<<<<<", cek,"<<<", indexScene)
        if(cek < indexScene){
              defaults.set(indexScene, forKey:"indexScene" )
        }
        
        return true
        
    }
    func changBackground() ->   Bool {
        
        if(storyData.scene[indexScene].choise){

            var subString:String = ""
            
            for (j,item )in storyData.scene[indexScene].isChoise.setIndex.enumerated(){
                
                
                let upperBound = storyData.scene[item].isBackground.index(storyData.scene[item].isBackground.endIndex, offsetBy: -1, limitedBy: storyData.scene[item].isBackground.startIndex) ?? storyData.scene[item].isBackground.startIndex
                if (getChoise == "B"){
                    subString = String(storyData.scene[item].isBackground[..<upperBound])  + "b"
                                        
                }else{
                    subString = String(storyData.scene[item].isBackground[..<upperBound])  + "a"
                }
                
                storyData.scene[item].isBackground = subString
                
            }
            
        }
        
        return true
    }
    
    func playStory() -> Bool {
        
        isNarasi.text = storyData.scene[indexScene].isNarasi
        dialogAyah.text = storyData.scene[indexScene].isDialogAyah
        objekModal.isHidden = !storyData.scene[indexScene].objekModal
        navigationBack.isHidden = !storyData.scene[indexScene].navigationBack
        navigationNext.isHidden = !storyData.scene[indexScene].navigationNext
        choiseA.isHidden = !storyData.scene[indexScene].choise
        choiseB.isHidden = !storyData.scene[indexScene].choise
        dialogAyah.isHidden = !storyData.scene[indexScene].dialogAyah
        
        backgroundGIF()
        objectClick()
        dynamicPositionNarasiLabel()
        
        if(indexScene ==  4){
            //print("masokkk")
            let imageA  = UIImage(named: "icon-gunung-choice")
            choiseA.setImage(imageA, for: .normal)
            let imageB  = UIImage(named: "icon-pantai-choice")
            choiseB.setImage(imageB, for: .normal)
        }
        else if(indexScene ==  14){
            let imageA  = UIImage(named: "icon-mobil-choice")
            choiseA.setImage(imageA, for: .normal)
            let imageB  = UIImage(named: "icon-bus-choice")
            choiseB.setImage(imageB, for: .normal)
        }
        
        if(storyData.scene[indexScene].choise){
            boxDialogAyah.image = UIImage(named: "boxdialog-kamping-budi")
            
            boxDialogAyah.isHidden = false
            
            }
        else{
            boxDialogAyah.image = UIImage(named: "boxdialog-kamping-ayah")
            boxDialogAyah.isHidden = !storyData.scene[indexScene].dialogAyah
        }
        
        if indexScene == 5 {
            if getChoise == "A" {
               dialogAyah.text = "Bagus Budi, ayah juga suka Gunung, kalau begitu ayo kita bersiap"
            }
            else if getChoise == "B" {
                dialogAyah.text = "Bagus Budi, ayah juga suka Pantai, kalau begitu ayo kita bersiap"
            }
        }
        
         saveIndex()
        return true
    }
    
    func dynamicPositionNarasiLabel() {
        isNarasi.textColor = UIColor.black
        
        if indexScene == 0 {
            isNarasi.frame = CGRect(x: 20, y: 250, width: 248, height: 100)
            isNarasi.numberOfLines = 4
            //isNarasi.lineBreakMode = .byWordWrapping
        }
        else if indexScene == 6 {
            isNarasi.frame = CGRect(x: 121, y: 660, width: 248, height: 100)
            isNarasi.textColor = UIColor.white
        }
        else if indexScene == 10 {
            isNarasi.frame = CGRect(x: 20, y: 71, width: 350, height: 300)
            isNarasi.textColor = UIColor.white
        }
        else if indexScene == 11 {
            isNarasi.frame = CGRect(x: 165, y: 278, width: 229, height: 100)
        }
        else if indexScene == 12 {
            isNarasi.frame = CGRect(x: 20, y: 65, width: 300, height: 300)
            isNarasi.textColor = UIColor.white
        }
        else if indexScene == 15 {
            isNarasi.frame = CGRect(x: 20, y: 150, width: 300, height: 300)
            isNarasi.textColor = UIColor.white
        }
        else if indexScene == 16 {
            isNarasi.frame = CGRect(x: 100, y: 140, width: 300, height: 300)
        }
        else if indexScene == 18 {
             isNarasi.frame = CGRect(x: 20, y: 191, width: 260, height: 200)
        }
        else if indexScene == 19 {
            isNarasi.frame = CGRect(x: 20, y: 50, width: 350, height: 450)
        }
    }
    
    func backgroundGIF() {
        let temp = UIImage.gif(name: storyData.scene[indexScene].isBackground )
               let img = isBackground
               
               if(indexScene == 0 || indexScene == 11 || indexScene == 20){
                   img!.animationImages = temp?.images
                   //img?.animationRepeatCount = 1
                   img!.startAnimating()
               }else{
                   img!.stopAnimating()
                   isBackground.image = UIImage(named: storyData.scene[indexScene].isBackground)
               }
    }
    
    func objectClick() {
        let buttonImage = UIImage.gif(name: storyData.scene[indexScene].isModal.iconImage)
               let buttonGIF = objekModal
               
               buttonGIF?.setImage(buttonImage, for: .normal)
               buttonGIF!.imageView?.animationImages = buttonImage?.images
               buttonGIF!.imageView?.startAnimating()
               
               if indexScene == 2 {
                   objekModal.frame = CGRect(x: 0, y: 207, width: 414, height: 551)
               }
               else if indexScene == 7 {
                   objekModal.frame = CGRect(x: 170, y: 390, width: 280, height: 259)
               }
               else if indexScene == 8 {
                   objekModal.frame = CGRect(x: -10, y: 63, width: 280, height: 259)
               }
               else if indexScene == 17 {
                   objekModal.frame = CGRect(x: 219, y: 503, width: 200, height: 259)
               }
               else {
                   buttonGIF!.imageView?.stopAnimating()
               }
    }
    
    func getData( ) -> String {
        
        if let path = Bundle.main.path(forResource: "barang", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                do{
                    
                    let jsonResponse = try JSONSerialization.jsonObject(with:
                        data, options: [])
                    
                    guard let jsonArray = jsonResponse as? [[String: Any]] else {
                        return "false"
                    }
                    
                    
                    for (i,item) in jsonArray.enumerated(){
                        var temp : (
                        isRead:Bool,
                        finish:Bool,
                        navigationNext:Bool,
                        navigationBack:Bool,
                        narasi:Bool,
                        dialogAyah:Bool,
                        objekModal:Bool,
                        choise:Bool,
                        isBackground:String,
                        isNarasi:String,
                        isDialogAyah:String,
                        isModal:
                        (
                        wordTitle:String,
                        photo:String,
                        isGif:String,
                        iconImage:String,
                        containerImage:[String]
                        ),
                        isChoise:(
                        setIndex:[Int],
                        changeBackground:[String],
                        toChange:Bool,
                        pattern:[String:String]
                        )
                        
                        )
                        
                        
                        // ================================================= BATAS LULUS TESTING DATA TANPA OPSIONAL
                        temp.isRead = item["isRead"] as? Bool ?? false
                        temp.finish   =   item["finish"] as? Bool ?? false
                        temp.navigationNext = item["navigationNext"] as? Bool ?? false
                        temp.navigationBack = item["navigationBack"] as? Bool ?? false
                        temp.narasi = item["narasi"] as? Bool ?? false
                        temp.dialogAyah = item["dialogAyah"]  as? Bool ?? false
                        temp.objekModal = item["objekModal"] as? Bool ?? false
                        temp.choise = item["choise"] as? Bool ?? false
                        temp.isBackground = item["isBackground"] as? String ?? ""
                        temp.isNarasi = item["isNarasi"] as? String ?? ""
                        temp.isDialogAyah = item["isDialogAyah"] as? String ?? ""
                        
                        // ================================================= BATAS LULUS TESTING DATA TANPA OPSIONAL
                        
                        let modal = item["isModal"] as? [String : Any]
                        
                        
                        temp.isModal.wordTitle = modal?["wordTitle"] as? String ?? ""
                        temp.isModal.photo = modal?["photo"] as? String ?? ""
                        temp.isModal.isGif = modal?["isGif"] as? String ?? ""
                        temp.isModal.iconImage = modal?["iconImage"] as? String ?? ""
                        temp.isModal.containerImage = modal?["containerImage"] as? [String] ?? []
                        
                        // ================================================= BATAS LULUS TESTING DATA TANPA OPSIONAL
                        
                        
                        //                        let choise = jsonArray[0]["isChoise"] as? [String : Any]
                        let choise = item["isChoise"] as? [String : Any]
                        
                        temp.isChoise.setIndex = choise?["setIndex"]  as? [Int] ?? []
                        
                        temp.isChoise.changeBackground = choise?["changeBackground"] as? [String] ?? []
                        temp.isChoise.toChange =  choise?["toChange"] as? Bool ?? false
                        
                        
                        
                        
                        let pattern = choise?["pattern"] as? Array<Any>  ?? [(
                            type:"",
                            iconImage:"",
                            setBackground:"",
                            wordTitle:""
                            )]
                        
                        
                        
                        storyData.scene.append(
                            (
                                isRead:temp.isRead,
                                finish:temp.finish,
                                navigationNext:temp.navigationNext,
                                navigationBack:temp.navigationBack,
                                narasi:temp.narasi,
                                dialogAyah:temp.dialogAyah,
                                objekModal:temp.objekModal,
                                choise:temp.choise,
                                isBackground:temp.isBackground,
                                isNarasi:temp.isNarasi,
                                isDialogAyah:temp.isDialogAyah,
                                isChoise:(setIndex: temp.isChoise.setIndex,
                                          changeBackground:    temp.isChoise.changeBackground,
                                          toChange: temp.isChoise.toChange,
                                          pattern: [
                                            ( type : " ", iconImage: " ", setBackground: " ", wordTitle: " " ),
                                            ( type : " ", iconImage: " ", setBackground: " ", wordTitle: " " )
                                ]),
                                isModal:temp.isModal))
                        
                        
                        
                    }
                    
                    
                    
                    
                } catch let parsingError {
                    print("Error", parsingError)
                }
            } catch {
                // Handle error here
                print("error")
            }
        }
        
        return "true"
        
    }
    
}
