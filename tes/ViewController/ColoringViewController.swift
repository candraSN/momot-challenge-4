//
//  ViewColoringController.swift
//  MembacaBersamaEja
//
//  Created by Candra Sabdana Nugroho on 01/06/20.
//  Copyright © 2020 William Sebastian Thedja. All rights reserved.
//

import UIKit

class ColoringViewController: UIViewController {
    
    @IBOutlet weak var wordImage: UIImageView!
    @IBOutlet weak var doneButton: UIButton!
    //    @IBOutlet weak var redButton: UIButton!
    //    @IBOutlet weak var pinkButton: UIButton!
    //    @IBOutlet weak var yellowButton: UIButton!
    //    @IBOutlet weak var greenButton: UIButton!
    //    @IBOutlet weak var blueButton: UIButton!
    //    @IBOutlet weak var dongkerButton: UIButton!
    //    @IBOutlet weak var purpleButton: UIButton!
    @IBOutlet var buttonColors: [UIButton]!
    
    var wordImageName = ""
    var objectName = ""
    var colorSelected = ""
    var buttonNotPressed: [UIImage?] = [
        UIImage(named: "tombol-merah"),
        UIImage(named: "tombol-pink"),
        UIImage(named: "tombol-kuning"),
        UIImage(named: "tombol-hijau"),
        UIImage(named: "tombol-biru"),
        UIImage(named: "tombol-dongker"),
        UIImage(named: "tombol-ungu")]
    
    var buttonPressed: [UIImage?] = [
        UIImage(named: "merah-lingkaran"),
        UIImage(named: "pink-lingkaran"),
        UIImage(named: "kuning-lingkaran"),
        UIImage(named: "hijau-lingkaran"),
        UIImage(named: "biru-lingkaran"),
        UIImage(named: "dongker-lingkaran"),
        UIImage(named: "ungu-lingkaran")]
    
    @IBAction func buttonColorsDidTap(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        switch sender.tag {
        case 0: sender.isSelected = sender.isSelected
        let color = "-merah"
        let realImg = wordImageName+color
        wordImage.image = UIImage(named: realImg)
        changeButton(index: 0)
           colorSelected = realImg
        
         
        case 1: sender.isSelected = sender.isSelected
        let color = "-pink"
        let realImg = wordImageName+color
        wordImage.image = UIImage(named: realImg)
        changeButton(index: 1)
             colorSelected = realImg
               
        case 2: sender.isSelected = sender.isSelected
        let color = "-kuning"
        let realImg = wordImageName+color
        wordImage.image = UIImage(named: realImg)
        changeButton(index: 2)
         colorSelected = realImg
              
        case 3: sender.isSelected = sender.isSelected
        let color = "-hijau"
        let realImg = wordImageName+color
        wordImage.image = UIImage(named: realImg)
        changeButton(index: 3)
         colorSelected = realImg
        case 4: sender.isSelected = sender.isSelected
        let color = "-biru"
        let realImg = wordImageName+color
        wordImage.image = UIImage(named: realImg)
        changeButton(index: 4)
         colorSelected = realImg
            
        case 5: sender.isSelected = sender.isSelected
        let color = "-dongker"
        let realImg = wordImageName+color
        wordImage.image = UIImage(named: realImg)
        changeButton(index: 5)
             colorSelected = realImg
            
        case 6: sender.isSelected = sender.isSelected
        let color = "-ungu"
        let realImg = wordImageName+color
        wordImage.image = UIImage(named: realImg)
        changeButton(index: 6)
         colorSelected = realImg
            
        default:
            return sender.isSelected = !sender.isSelected
        }
    }
    
    
    func changeButton(index:Int) {
        for item in 0...6 {
            if item == index {
                buttonColors[item].setImage(buttonPressed[item], for: .normal)
            }
            else {
                buttonColors[item].setImage(buttonNotPressed[item], for: .normal)
            }
        }
    }
    
    @IBAction func toSave(_ sender: Any) {
                
        let dialogMessage = UIAlertController(title: "Konfirmasi", message: "Apakah kamu yakin ingin mengganti warna objek ini ?", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Ya", style: .default, handler: {(action) -> Void in
        let defaults = UserDefaults.standard
        //        var cek = defaults.object(forKey: "koleksi\(wordImageName)") as? String ?? "kosong"
        //        if(cek == "kosong"){
            defaults.set(self.colorSelected, forKey:"koleksi\(self.wordImageName)" )
        })
        
        let cancel = UIAlertAction(title: "Tidak", style: .cancel, handler: {(action) -> Void in })
                   
                   dialogMessage.addAction(ok)
                   dialogMessage.addAction(cancel)
                   
                   self.present(dialogMessage, animated: true, completion: nil)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = UserDefaults.standard
        let red = "-merah"
              let realImg = wordImageName+red
        let loadSavedImage = defaults.object(forKey:"koleksi\(wordImageName)") as? String ?? realImg
        
 
        
        wordImage.image = UIImage(named: loadSavedImage)
        
        // Do any additional setup after loading the view.
    }
    
    //    func startColor() -> Bool {
    //
    //        let allButtonCollor = [
    //            (
    //                button:redButton,
    //                color:"E81C24"
    //            ),
    //            (
    //                button:pinkButton,
    //                color:"E20082"
    //            ),
    //            (
    //                button:yellowButton,
    //                color:"FFE800"
    //            ),
    //            (
    //                button:greenButton,
    //                color:"009C51"
    //            ),
    //            (
    //                button:blueButton,
    //                color:"00A4E5"
    //            ),
    //            (
    //                button:donkerButton,
    //                color:"2E3188"
    //            ),
    //            (
    //                button:purpleButton,
    //                color:"9261EE"
    //            )
    //        ]
    //
    //        for item in allButtonCollor{
    //            item.button?.backgroundColor = getUIColor(hex:item.color)
    //            item.button?.setTitle("" , for: .normal)
    //            item.button?.layer.cornerRadius = 15
    //            item.button?.layer.borderWidth = 0.3
    //            item.button?.layer.borderColor = UIColor.black.cgColor
    //        }
    //
    //        return true
    //    }
    //
    //    func getUIColor(hex: String, alpha: Double = 1.0) -> UIColor? {
    //        var cleanString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    //
    //        if (cleanString.hasPrefix("#")) {
    //            cleanString.remove(at: cleanString.startIndex)
    //        }
    //
    //        if ((cleanString.count) != 6) {
    //            return nil
    //        }
    //
    //        var rgbValue: UInt32 = 0
    //        Scanner(string: cleanString).scanHexInt32(&rgbValue)
    //
    //        return UIColor(
    //            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
    //            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
    //            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
    //            alpha: CGFloat(1.0)
    //        )
    //    }
    
    //    @IBAction func redButtonDidTap(_ sender: Any) {
    //        redButton.setImage(UIImage(named: "merah-lingkaran"), for: .normal)
    //        let color = "-merah"
    //        let realImg = wordImageName+color
    //        wordImage.image = UIImage(named: realImg)
    //    }
    //
    //    @IBAction func pinkButtonDidTap(_ sender: Any) {
    //        let color = "-pink"
    //        let realImg = wordImageName+color
    //
    //        wordImage.image = UIImage(named: realImg)
    //    }
    //
    //    @IBAction func yellowButtonDidTap(_ sender: Any) {
    //        let color = "-kuning"
    //        let realImg = wordImageName+color
    //
    //        wordImage.image = UIImage(named: realImg)
    //    }
    //
    //    @IBAction func greenButtonDidTap(_ sender: Any) {
    //        let color = "-hijau"
    //        let realImg = wordImageName+color
    //
    //        wordImage.image = UIImage(named: realImg)
    //    }
    //
    //    @IBAction func blueButtonDidTap(_ sender: Any) {
    //        let color = "-biru"
    //        let realImg = wordImageName+color
    //
    //        wordImage.image = UIImage(named: realImg)
    //    }
    //
    //    @IBAction func donkerButtonDidTap(_ sender: Any) {
    //        let color = "-dongker"
    //        let realImg = wordImageName+color
    //
    //        wordImage.image = UIImage(named: realImg)
    //    }
    //
    //    @IBAction func purpleButtonDidTap(_ sender: Any) {
    //        let color = "-ungu"
    //        let realImg = wordImageName+color
    //
    //        wordImage.image = UIImage(named: realImg)
    //    }
    
}
