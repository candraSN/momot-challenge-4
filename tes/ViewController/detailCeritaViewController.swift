//
//  detailCeritaViewController.swift
//  tes
//
//  Created by Jeslyn Kosasih on 17/06/20.
//  Copyright © 2020 afitra mamor. All rights reserved.
//

import UIKit

class detailCeritaViewController: UIViewController {

    @IBOutlet weak var titleStoryLabel: UILabel!
    @IBOutlet weak var narasiLabel: UILabel!
    @IBOutlet weak var tokohLabel: UILabel!
    
    @IBOutlet weak var progresBar: UIProgressView!
    @IBOutlet weak var viewModal: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
          let defaults = UserDefaults.standard
                var cek = defaults.object(forKey: "indexScene") as? Int ?? 0
 print("ini maksdnya",Float(Double(cek) / 20.0))
           progresBar.progressTintColor = getUIColor(hex:"b6eb7a")
progresBar.transform = progresBar.transform.scaledBy(x: 1, y: 10)
        progresBar.progress = Float(Double(cek) / 20.0)
        
        progresBar.layer.cornerRadius = 8
        progresBar.clipsToBounds = true
        progresBar.layer.sublayers![1].cornerRadius = 8
        progresBar.subviews[1].clipsToBounds = true

        viewModal.layer.cornerRadius = 30
        
        titleStoryLabel.text = "Kamping"
        narasiLabel.text = "Suatu hari, Budi diajak ayahnya pergi berkemah dengan ibu. Pada saat berkemah Budi melihat sesuatu dan ayahnya pun mengajarkan Budi untuk menjaga kebersihan lingkungan"
        tokohLabel.text = "Orangtua menjadi narator dan ayah anak "
        // Do any additional setup after loading the view.
    }
    
        func getUIColor(hex: String, alpha: Double = 1.0) -> UIColor? {
             var cleanString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
     
             if (cleanString.hasPrefix("#")) {
                 cleanString.remove(at: cleanString.startIndex)
             }
     
             if ((cleanString.count) != 6) {
                 return nil
             }
     
             var rgbValue: UInt32 = 0
             Scanner(string: cleanString).scanHexInt32(&rgbValue)
     
             return UIColor(
                 red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                 green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                 blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                 alpha: CGFloat(1.0)
             )
         }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
