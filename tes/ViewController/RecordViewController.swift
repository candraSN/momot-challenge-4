//
//  ViewRecordController.swift
//  MembacaBersamaEja
//
//  Created by Candra Sabdana Nugroho on 01/06/20.
//  Copyright © 2020 William Sebastian Thedja. All rights reserved.
//

import UIKit
import AVFoundation

class RecordViewController: UIViewController, AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    var recordingSesstion: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer: AVAudioPlayer!
    
    var numberOfRecords:Int = 0
    var objectName = ""
    
    var asyikFileName: [String] = []
    var bajuFileName: [String] = []
    var ranselFileName: [String] = []
    var tendaFileName: [String] = []
    
    private let spacing:CGFloat = 16.0
    
    @IBOutlet weak var recordsCollectionView: UICollectionView!
    
    @IBOutlet weak var recordButton: UIButton!
    
    @IBAction func recordDidTap(_ sender: Any) {
        if audioRecorder == nil {
            numberOfRecords += 1
            //saveObjekName = "\(objek)\(numberOfRecords)"
            let fileName = getDirectory().appendingPathComponent("\(objectName)\(numberOfRecords).m4a")
            let settings = [AVFormatIDKey: Int(kAudioFormatMPEG4AAC), AVSampleRateKey: 12000, AVNumberOfChannelsKey: 1, AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue]
            
            do {
                audioRecorder = try AVAudioRecorder(url: fileName, settings: settings)
                audioRecorder.delegate = self
                audioRecorder.record()
                
                recordButton.setImage(UIImage(named: "stopRecord"), for: .normal)
            }
            catch {
                displayAlert(title: "Oops!", message: "Rekamanmu gagal")
            }
        }
        else {
            audioRecorder.stop()
            audioRecorder = nil
            
            if objectName.lowercased() == "asyik" {
                asyikFileName.append("\(objectName)\(numberOfRecords)")
                UserDefaults.standard.set(asyikFileName, forKey: "asyik")
            }
            else if objectName.lowercased() == "baju" {
                bajuFileName.append("\(objectName)\(numberOfRecords)")
                UserDefaults.standard.set(bajuFileName, forKey: "baju")
            }
            else if objectName.lowercased() == "ransel" {
                ranselFileName.append("\(objectName)\(numberOfRecords)")
                UserDefaults.standard.set(ranselFileName, forKey: "ransel")
            }
            else if objectName.lowercased() == "tenda" {
                tendaFileName.append("\(objectName)\(numberOfRecords)")
                UserDefaults.standard.set(tendaFileName, forKey: "tenda")
            }
            
            recordButton.setImage(UIImage(named: "startRecord"), for: .normal)
            recordsCollectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupPermission()
        
        recordsCollectionView.delegate = self
        recordsCollectionView.dataSource = self
        
        let layout = recordsCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
    }
    
    func getDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = paths[0]
        return documentDirectory
    }
    
    func displayAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "dismiss", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func setupPermission() {
        recordingSesstion = AVAudioSession.sharedInstance()
        print()
        if objectName.lowercased() == "asyik"{
            if let saveObject: [String] = UserDefaults.standard.object(forKey: "asyik") as? [String] {
                asyikFileName = saveObject
            }
        }else if objectName.lowercased() == "baju" {
            if let saveObject: [String] = UserDefaults.standard.object(forKey: "baju") as? [String] {
                bajuFileName = saveObject
            }
        }else if objectName.lowercased() == "ransel" {
            if let saveObject: [String] = UserDefaults.standard.object(forKey: "ransel") as? [String] {
                ranselFileName = saveObject
            }
        }else if objectName.lowercased() == "tenda" {
            if let saveObject: [String] = UserDefaults.standard.object(forKey: "tenda") as? [String] {
                tendaFileName = saveObject
            }
        }
        
        AVAudioSession.sharedInstance().requestRecordPermission { (hasPermission) in
            if hasPermission {
                print("ACCEPTED")
            }
        }
    }
}

extension RecordViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if objectName.lowercased() == "asyik" {
            return asyikFileName.count
        }
        else if objectName.lowercased() == "baju" {
            return bajuFileName.count
        }
        else if objectName.lowercased() == "ransel" {
            return ranselFileName.count
        }
        else if objectName.lowercased() == "tenda" {
            return tendaFileName.count
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM yyyy"
        let dateInFormat = dateFormatter.string(from: NSDate() as Date)
        
        let cell = recordsCollectionView.dequeueReusableCell(withReuseIdentifier: "RecordCell", for: indexPath) as! RecordCell
        cell.nameRecordLabel.text = "Rekaman \(String(indexPath.row + 1))"
        cell.recordImage.image = UIImage(named: "rekaman")
        cell.recordTimeLabel.text = dateInFormat
        cell.delegate = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if objectName.lowercased() == "asyik" {
            let path = getDirectory().appendingPathComponent("\(asyikFileName[indexPath.item]).m4a")
            
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: path)
                audioPlayer.play()
            }
            catch {
                
            }
        }
        else if objectName.lowercased() == "baju" {
            let path = getDirectory().appendingPathComponent("\(bajuFileName[indexPath.item]).m4a")
            
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: path)
                audioPlayer.play()
            }
            catch {
                
            }
        }
        else if objectName.lowercased() == "ransel" {
            let path = getDirectory().appendingPathComponent("\(ranselFileName[indexPath.item]).m4a")
            
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: path)
                audioPlayer.play()
            }
            catch {
                
            }
        }
        else if objectName.lowercased() == "tenda" {
            let path = getDirectory().appendingPathComponent("\(tendaFileName[indexPath.item]).m4a")
            
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: path)
                audioPlayer.play()
            }
            catch {
                
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfItemsPerRow:CGFloat = 2
        let spacingBetweenCells:CGFloat = 16
        
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        
        if let collection = self.recordsCollectionView{
            let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
            return CGSize(width: width, height: width)
        }else{
            return CGSize(width: 0, height: 0)
        }
    }
    
}

extension RecordViewController: RecordCellDelegate {
    func delete(cell: RecordCell) {
        if let indexPath = recordsCollectionView.indexPath(for: cell) {
            
            let dialogMessage = UIAlertController(title: "Konfirmasi", message: "Apakah kamu yakin ingin menghapus rekaman suara ini ?", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Ya", style: .default, handler: {(action) -> Void in
                
                if self.objectName.lowercased() == "asyik" {
                    self.asyikFileName.remove(at: indexPath.item)
                    self.recordsCollectionView.deleteItems(at: [indexPath])
                }
                else if self.objectName.lowercased() == "baju" {
                    self.bajuFileName.remove(at: indexPath.item)
                    self.recordsCollectionView.deleteItems(at: [indexPath])
                }
                else if self.objectName.lowercased() == "ransel" {
                    self.ranselFileName.remove(at: indexPath.item)
                    self.recordsCollectionView.deleteItems(at: [indexPath])
                }
                else if self.objectName.lowercased() == "tenda" {
                    self.tendaFileName.remove(at: indexPath.item)
                    self.recordsCollectionView.deleteItems(at: [indexPath])
                }
            })
            
            let cancel = UIAlertAction(title: "Tidak", style: .cancel, handler: {(action) -> Void in })
            
            dialogMessage.addAction(ok)
            dialogMessage.addAction(cancel)
            
            self.present(dialogMessage, animated: true, completion: nil)
            
        }
    }
}

//    @IBOutlet weak var recordButton: UIButton!
//    @IBOutlet weak var collectionView: UICollectionView!
//
//    let stopRecordImage = UIImage(named: "stopRecord")
//    let startRecordImage = UIImage(named: "startRecord")
//    let normalButton = UIImage(named: "play")
//    let stopButton = UIImage(named: "stop")
//
//    var soundRecorder = AVAudioRecorder()
//    var soundPlayer = AVAudioPlayer()
//    var permisionrecord = AVAudioSession()
//
//    var numberOfRecords = 0
//    //var formatFile : String = ".m4a"
//    var namaBarang = ""
//
//
////    var dataDummy = []
//    var data1:[(name:String, record:[String])] = []
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        //nameFile = "Objek\(numberOfRecords)\(formatFile)"
//
//        if let file: [(name:String, record:[String])] = UserDefaults.standard.object(forKey: "myNumber") as? [(name:String, record:[String])] {
//            data1 = file
//        }
//
//        setupPermission()
//        // Do any additional setup after loading the view.
//    }
//
//    func setupPermission() {
//        permisionrecord = AVAudioSession.sharedInstance()
//        do{
//            try permisionrecord.setCategory(.playAndRecord, mode: .default)
//            try permisionrecord.setActive(true)
//            permisionrecord.requestRecordPermission(){ [unowned self] allowed in
//                DispatchQueue.main.async {
//                    if allowed {
//
//                            self.setUpRecorder()
//                    } else {
//                        self.displayAlert(title: "Ups!", message: "Rekam suaramu gagal")
//                    }
//                }
//            }
//        }
//        catch{
//            self.displayAlert(title: "Ups!", message: "Rekam suaramu gagal")
//        }
//    }
//
//    func getDocumentDirector() -> URL {
//        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
//        return path[0]
//    }
//
//
////    func loadFailUI() {
////        let failLabel = UILabel()
////        failLabel.font = UIFont.preferredFont(forTextStyle: .headline)
////        failLabel.text = "Recording failed: please ensure the app has access to your microphone."
////        failLabel.numberOfLines = 0
////
////
////    }
//
//
//    func setUpRecorder() {
//        let audioFileName = getDocumentDirector().appendingPathComponent("\(namaBarang)-record-\(numberOfRecords).m4a")
//        print(audioFileName.absoluteString)
//        let recordSetting = [AVFormatIDKey: kAudioFormatAppleLossless,
//                             AVEncoderAudioQualityKey : AVAudioQuality.max.rawValue,
//                             AVEncoderBitRateKey : 320000,
//                             AVNumberOfChannelsKey : 2,
//                             AVSampleRateKey : 44100]as [String : Any]
//
//        do{
//            soundRecorder = try AVAudioRecorder(url: audioFileName, settings: recordSetting)
//            soundRecorder.delegate = self
//            soundRecorder.prepareToRecord()
//        }
//        catch{
//            print(error)
//        }
//    }
//
////    func setUpPlayer() {
////        let audioFileName = getDocumentDirector().appendingPathComponent(nameFile)
////        do{
////            soundPlayer = try AVAudioPlayer(contentsOf: audioFileName)
////            soundPlayer.delegate = self
////            soundPlayer.prepareToPlay()
////            soundPlayer.volume = 3.0
////        }
////        catch{
////            print(error)
////        }
////    }
//
//
//
//    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
//        recordButton.isEnabled = true
//        //playRecordButton.setImage(normalButton, for: .normal)
//        //playRecordButton.setTitle("Play", for: .normal)
//    }
//
//    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
//        //playRecordButton.isEnabled = true
//    }
//
//    func displayAlert(title: String, message: String) {
//        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
//        present(alert, animated: true, completion: nil)
//    }
//
//
//
//    @IBAction func recordBtn(_ sender: Any) {
//
//            if recordButton.titleLabel?.text == "Record" {
//                numberOfRecords += 1
//                setUpRecorder()
//                recordButton.setImage(stopRecordImage, for: .normal)
//                recordButton.setTitle("Stop", for: .normal)
//                //playRecordButton.isEnabled = false
//
//        }
//            else{
//                soundRecorder.stop()
//                UserDefaults.standard.set(numberOfRecords, forKey: "myNumber")
//                recordButton.setImage(startRecordImage, for: .normal)
//                recordButton.setTitle("Record", for: .normal)
//                //playRecordButton.isEnabled = false
//        }
//
//    }
//
////    @IBAction func playBtn(_ sender: Any) {
////        if playRecordButton.titleLabel?.text == "Play" {
////            playRecordButton.setImage(stopButton, for: .normal)
////            playRecordButton.setTitle("Stop", for: .normal)
////            recordButton.isEnabled = false
////            setUpPlayer()
////            soundPlayer.play()
////        }
////        else{
////            soundPlayer.stop()
////            playRecordButton.setImage(normalButton, for: .normal)
////            playRecordButton.setTitle("Play", for: .normal)
////            recordButton.isEnabled = false
////
////        }
////    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return data1[section].record.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recordCell", for: indexPath) as! RecordCell
//        cell.labelRecord.text = data1[0].record[indexPath.item]
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let audioFileName = getDocumentDirector().appendingPathComponent( "\(namaBarang)-record-\(numberOfRecords).m4a")
//        do{
//            soundPlayer = try AVAudioPlayer(contentsOf: audioFileName)
//            soundPlayer.delegate = self
//            soundPlayer.play()
//            soundPlayer.volume = 3.0
//        }
//        catch{
//            print(error)
//        }
//    }

