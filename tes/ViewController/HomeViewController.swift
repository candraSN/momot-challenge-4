//
//  HomeViewControl.swift
//  tes
//
//  Created by afitra mamor on 27/05/20.
//  Copyright © 2020 afitra mamor. All rights reserved.
//

import Foundation
import UIKit

class HomeViewController: UIViewController {
    
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var ResultTable: UITableView!
    
    @IBOutlet weak var StoryCollection: UICollectionView!
    
    @IBOutlet weak var CategoryCollection: UICollectionView!
    
    @IBOutlet weak var nameLabel: UINavigationItem!
    
    @IBOutlet weak var rekomendasiLabel: UILabel!
    @IBOutlet weak var kampingLabel: UILabel!
    
    @IBOutlet weak var lainnyaLabel: UIButton!
    @IBOutlet weak var kategoriLabel: UILabel!
    
    var allData = [(title:"kamping",category:"petualangan",image:"tes-image")]
    var dataFilter :[(title:String,category:String,image:String)] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nama = Helpers().getSession()[0].nama
        nameLabel.title = "Hai, \(nama) !"
        
        ResultTable.isHidden = true
        StoryCollection.delegate = self
        StoryCollection.dataSource = self
        
        CategoryCollection.delegate = self
        CategoryCollection.dataSource = self
        
        searchBar.delegate = self
        
        ResultTable.delegate = self
        ResultTable.dataSource = self
        
        let layout = CategoryCollection.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func unwindToScreenBefore (_ unwindSegue: UIStoryboardSegue) {
             
         }
//
//    @IBAction func take(_ sender: Any) {
//
//        StoryCollection.isHidden=true
//        
//    }
    
    
    func changeShow(input:Bool) ->Bool {
        if(input){
            StoryCollection.isHidden = true
            
            CategoryCollection.isHidden = true
            rekomendasiLabel.isHidden = true
            kampingLabel.isHidden = true
            kategoriLabel.isHidden = true
            lainnyaLabel.isHidden = true
            ResultTable.isHidden=false
            
        }else{
            StoryCollection.isHidden = false
            
            CategoryCollection.isHidden = false
            ResultTable.isHidden=true
            rekomendasiLabel.isHidden = false
            kampingLabel.isHidden = false
            kategoriLabel.isHidden = false
            lainnyaLabel.isHidden = false
        }
        return true
    }
}



extension HomeViewController: UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == StoryCollection {
            return 1
        }
        else if collectionView == CategoryCollection {
            return 1
        }
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "toDetail", sender: self)
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 1
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView == StoryCollection ){
            
            let cellCostom = collectionView.dequeueReusableCell(withReuseIdentifier: "StoryHomeCell", for: indexPath) as! StoryViewCell
            
            cellCostom.photoLabel.image=UIImage(named: "Group")
            
            return cellCostom
            
        }else  {
            
            let cellCostom = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryHomeCell", for: indexPath) as! CategoryViewCell
            
            cellCostom.photoLabel.image=UIImage(named: "kategori")
            //                                        cellCostom.layer.cornerRadius = 5
            cellCostom.layer.masksToBounds = true
            return cellCostom
        }
        
        
    }
    

}

extension HomeViewController :  UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if(collectionView == StoryCollection ){
            return UIEdgeInsets(top: 0, left:0, bottom: 0, right: 0)
        }else{
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bounds = collectionView.bounds
        let heightVal = self.view.frame.height
        let widthVal = self.view.frame.width
        let cellsize = (heightVal < widthVal) ?  bounds.height/2 : bounds.width/2
        
        if(collectionView == StoryCollection ){
            return CGSize(width: cellsize + 90  , height: cellsize+0  )
        }else{
            return CGSize(width: cellsize - 1   , height:  cellsize - 80 )
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}


extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let cellCostom = tableView.dequeueReusableCell(withIdentifier: "SearchHomeCell") as! SearchTableCell
        
        cellCostom.titleLabel.text = dataFilter[indexPath.row].title
        cellCostom.categoryLabel.text =  dataFilter[indexPath.row].category
        cellCostom.photoLabel.image=UIImage(named: dataFilter[indexPath.row].image)
        
        
        return cellCostom
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //indexint = indexPath.row
        //print(indexint)
        performSegue(withIdentifier: "toDetail", sender: self)
        
    }
}


extension HomeViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        if(searchText.count>0){
            changeShow(input:true)
            var input = searchBar.text
            var result :[(title:String,category:String,image:String)] = []
            
            for item in allData {
                var temp :String = ""
                var flag :Bool = false
                
                
                
                for i in 0 ..< item.title.count {
                    var max = input?.count ?? 0
                    var start = item.title.index(item.title.startIndex, offsetBy: i)
                    
                    if((start.encodedOffset + max ) < item.title.count){
                        
                        var end = item.title.index(item.title.startIndex, offsetBy: (start.encodedOffset + max))
                        let range = start..<end
                        
                        temp = String(item.title[range])
                        
                    }
                    if(temp.lowercased() == input?.lowercased()){
                        flag = true
                    }
                }
                
                
                if(flag){
                    result.append(item)
                }
            }
            
            
            result =  result.sorted { $0.title.lowercased() < $1.title.lowercased() }
            dataFilter = result
       
            
            ResultTable.reloadData()
        }else{
            changeShow(input:false)
        }
        
        
    }
}


