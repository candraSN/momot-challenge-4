//
//  Helpers.swift
//  tes
//
//  Created by afitra mamor on 02/06/20.
//  Copyright © 2020 afitra mamor. All rights reserved.
//

import Foundation
import UIKit
import CoreData
struct DataOnStorage {
    
    let status: Bool
    let objekName: String
}

struct SessionOnStage {
    
    let status: Bool
    let nama:String
    
}

class Helpers  {
    
    
    func createSession( _ nama:String  ,_ status:Bool){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        // managed context
        let managedContext = appDelegate.persistentContainer.viewContext
        let userEntity = NSEntityDescription.entity(forEntityName: "Sessions", in: managedContext)!
        let insert = NSManagedObject(entity: userEntity, insertInto: managedContext)
        insert.setValue(status, forKey: "status")
        insert.setValue(nama, forKey: "nama")
        
        do{
            // save data ke entity user core data
            try managedContext.save()
            
            
        }catch let _{
            print("eroooooor")
            
        }
    }
    
    func getSession() -> [SessionOnStage]{
        
        var users = [SessionOnStage]()
        
        // referensi ke AppDelegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        // managed context
        let managedContext = appDelegate.persistentContainer.viewContext
        
        // fetch data
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Sessions")
        
        do{
            let result = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            result.forEach{ user in
                users.append(
                    SessionOnStage(
                        status: user.value(forKey: "status") as! Bool, nama: user.value(forKey: "nama") as! String
                    )
                )
            }
            
        }catch let err{
            print(err)
        }
        
        return users
        
    }
    
    func create(  _ objekName:String, _ status:Bool){

        let allData = retrieve()
        
        
        var flag = true
        for (i,item) in allData.enumerated(){
            
            if(item.objekName == objekName){
                flag = false
            }
            
            
        }
        
        if(flag){
            
            // referensi ke AppDelegate
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            
            // managed context
            let managedContext = appDelegate.persistentContainer.viewContext
            
            
            
            let userEntity = NSEntityDescription.entity(forEntityName: "Users", in: managedContext)!
            
            
            
            let insert = NSManagedObject(entity: userEntity, insertInto: managedContext)
            insert.setValue(status, forKey: "status")
            insert.setValue(objekName, forKey: "objekName")
            do{
                // save data ke entity user core data
                try managedContext.save()
                
                
            }catch let err{
                print("eroooooor")
                
            }
        }
        
        
    }
    func retrieve() -> [DataOnStorage]{
        
        var users = [DataOnStorage]()
        
        // referensi ke AppDelegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        // managed context
        let managedContext = appDelegate.persistentContainer.viewContext
        
        // fetch data
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        
        do{
            let result = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            result.forEach{ user in
                users.append(
                    DataOnStorage(
                        status: user.value(forKey: "status") as! Bool, objekName: user.value(forKey: "objekName") as! String
                    )
                )
            }
            
        }catch let err{
            print(err)
        }
        
        return users
        
    }
    func readSession() -> [SessionOnStage]{
           
           var session = [SessionOnStage]()
           
           // referensi ke AppDelegate
           let appDelegate = UIApplication.shared.delegate as! AppDelegate
           
           // managed context
           let managedContext = appDelegate.persistentContainer.viewContext
           
           // fetch data
           let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Sessions")
           
           do{
               let result = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
               result.forEach{ user in
                   session.append(
                       SessionOnStage(
                           status: user.value(forKey: "status") as! Bool, nama: user.value(forKey: "nama") as! String
                       )
                   )
               }
               
           }catch let err{
               print(err)
           }
           
           return session
           
       }
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}
